FROM rust:latest as builder

WORKDIR /usr/src/paper-server
COPY . .

RUN cargo build --release

FROM debian:10-slim
ENV LANG C.UTF-8
LABEL maintainer "AveryanAlex <averyanalex@gmail.com>"


COPY --from=builder /usr/src/paper-server/target/release/server /usr/local/bin
CMD ["server"]

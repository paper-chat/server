#[macro_use] extern crate rocket;

#[get("/hello/<name>/<age>", rank = 2)]
async fn hello(name: &str, age: u8) -> String {
    tokio::time::sleep(std::time::Duration::new(5, 0)).await;
    format!("Hello, {} year old named {}!", age, name)
}

#[get("/goodbye/<name>/<age>", rank = 1)]
async fn goodbye(name: &str, age: u8) -> String {
    format!("Goodbye, {} year old named {}!", age, name)
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![hello, goodbye])
}
